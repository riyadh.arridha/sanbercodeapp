import React from 'react';
import {Intro, Biodata, TodoList, TodoContext, Splash} from './tugas';
import {Quiz1} from './quiz/';
import firebase from '@react-native-firebase/app';

var firebaseConfig = {
  apiKey: "AIzaSyBIgWESdWRVO_r6YMMO5dedKxMbl3cNmB4",
  authDomain: "riyadhsanbercodeapp.firebaseapp.com",
  databaseURL: "https://riyadhsanbercodeapp.firebaseio.com",
  projectId: "riyadhsanbercodeapp",
  storageBucket: "riyadhsanbercodeapp.appspot.com",
  messagingSenderId: "331199580211",
  appId: "1:331199580211:web:ba476f2944b59b66b6fb46",
  measurementId: "G-XQW93XPXEC"
};

// Inisialisasi firebase
if(!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}

const App = () => {
  // return <Intro />;
  // return <Biodata />;
  // return <TodoList />;
  // return <TodoContext />;
  // return <Quiz1 />;
  return <Splash />;
};
export default App;
