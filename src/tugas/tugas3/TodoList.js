import React from 'react';
import {View, TouchableOpacity, Text, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
const TodoList = (props) => (
  <View style={styles.taskWrapper}>
    <View style={styles.lisTask}>
      <TouchableOpacity onPress={() => props.setChecked()}>
        <Icon
          name={props.checked ? 'check' : 'square'}
          size={25}
          color="#1c1c1c"
          style={styles.iconChecked}
        />
      </TouchableOpacity>

      <View style={styles.lineView}>
        {props.checked && <View style={styles.verticalLine}></View>}
        <Text style={styles.task}>{props.text}</Text>
      </View>
    </View>
    <View style={styles.iconTrash}>
      <Icon name="trash-2" size={25} color="#1c1c1c" onPress={props.delete} />
    </View>
  </View>
);

export default TodoList;

const styles = StyleSheet.create({
  taskWrapper: {
    marginTop: '5%',
    flexDirection: 'row',
    borderColor: '#c9c9c9',
    borderWidth: 4,
    width: '100%',
    alignItems: 'stretch',
    minHeight: 40,
    borderRadius: 6,
  },
  lisTask: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '90%',
  },
  iconChecked: {
    marginLeft: 10,
    paddingVertical: 20,
    marginRight: 5,
  },
  task: {
    paddingVertical: 20,
    borderColor: '#1c1c1c',
    color: '#1c1c1c',
  },
  lineView: {
    width: '85%',
    justifyContent: 'center',
  },
  verticalLine: {
    borderBottomColor: '#1c1c1c',
    borderBottomWidth: 3,
    width: '100%',
    position: 'absolute',
  },
  iconTrash: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
