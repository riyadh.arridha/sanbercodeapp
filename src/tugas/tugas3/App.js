import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  // ImageBackground,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import TodoList from './TodoList';

const date = new Date();

const App = () => {
  const [value, setValue] = useState('');
  const [todos, setTodos] = useState([]);

  const handleAddTodo = () => {
    const tahun = date.getFullYear();
    const bulan = date.getMonth();
    const tanggal = date.getDate();
    // console.log(year);
    if (value.length > 0) {
      setTodos([
        ...todos,
        {
          text: `${tanggal}/${bulan}/${tahun} \n${value}`,
          key: Date.now(),
          checked: false,
        },
      ]);
      setValue('');
    }
  };

  const handleDeleteTodo = (id) => {
    setTodos(
      todos.filter((todo) => {
        if (todo.key !== id) return true;
      }),
    );
  };

  const handleChecked = (id) => {
    setTodos(
      todos.map((todo) => {
        if (todo.key === id) todo.checked = !todo.checked;
        return todo;
      }),
    );
  };
  return (
    // <ImageBackground
    //   style={{width: '100%', height: '100%', flex: 1}}
    //   source={require('./BackgroudColor.jpg')}>
    <View style={styles.container}>
      <Text style={{fontSize: 16, color: '#1c1c1c', paddingBottom: 10}}>
        Masukkan Todolist
      </Text>
      <View style={styles.textInputContainer}>
        <TextInput
          style={styles.textInput}
          multiline={true}
          onChangeText={(value) => setValue(value)}
          placeholder={'input here'}
          placeholderTextColor="#a9a9a9"
          color="#1c1c1c"
          value={value}
        />

        <TouchableOpacity onPress={() => handleAddTodo()}>
          <View style={styles.plusIcon}>
            <Icon name="plus" size={30} color="#1c1c1c" />
          </View>
        </TouchableOpacity>
      </View>
      <ScrollView>
        {todos.map((todo) => (
          <TodoList
            text={todo.text}
            key={todo.key}
            checked={todo.checked}
            setChecked={() => handleChecked(todo.key)}
            delete={() => handleDeleteTodo(todo.key)}
          />
        ))}
      </ScrollView>
    </View>
    // </ImageBackground>
  );
};
export default App;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 15,
  },
  plusIcon: {
    backgroundColor: '#3dc6ff',
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textInput: {
    flex: 1,
    minHeight: '7%',
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: '#1c1c1c',
    marginRight: 10,
  },
  // todoWrapper: {
  //   flexDirection: 'row',
  //   borderColor: '#D0D0D0',
  //   borderBottomWidth: 0.5,
  //   width: '100%',
  //   alignItems: 'stretch',
  //   minHeight: 40,
  // },
  // todo: {
  //   paddingBottom: 20,
  //   paddingLeft: 10,
  //   paddingTop: 6,
  //   borderColor: 'white',
  //   borderBottomWidth: 1,
  //   fontSize: 17,
  //   fontWeight: 'bold',
  //   color: 'white',
  // },
  textInputContainer: {
    flexDirection: 'row',
  },
});
