import React, {useState, createContext} from 'react';
import {View, Text} from 'react-native';
import TodoList from './todoList';

export const RootContext = createContext();

const Context = () => {
  const [value, setValue] = useState('');
  const [todos, setTodos] = useState([]);

  //   const handleChangeInput = (input) => {
  //     setValue(input);
  //   };

  const handleAddTodo = () => {
    const date = new Date();
    const tahun = date.getFullYear();
    const bulan = date.getMonth();
    const tanggal = date.getDate();
    // console.log(year);
    if (value.length > 0) {
      setTodos([
        ...todos,
        {
          text: `${tanggal}/${bulan}/${tahun} \n${value}`,
          key: Date.now(),
          checked: false,
        },
      ]);
      setValue('');
    }
  };

  const handleDeleteTodo = (id) => {
    setTodos(
      todos.filter((todo) => {
        if (todo.key !== id) return true;
      }),
    );
  };

  return (
    <RootContext.Provider
      value={{
        value,
        setValue,
        todos,
        setTodos,
        // handleChangeInput,
        handleAddTodo,
        handleDeleteTodo,
      }}>
      <TodoList />
    </RootContext.Provider>
  );
};

export default Context;
