import React, {useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {RootContext} from '.';

const TodoList = () => {
  const state = useContext(RootContext);
  console.log('TodoList -> State', state);

  const renderItem = ({item, index}) => {
    return (
      <View style={styles.taskWrapper}>
        <View style={styles.lisTask}>
          <Text style={styles.task}>{item.text}</Text>
        </View>
        <View style={styles.iconTrash}>
          <Icon
            name="trash-2"
            size={25}
            color="#1c1c1c"
            onPress={() => state.handleDeleteTodo(item.key)}
          />
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Text style={{fontSize: 16, color: '#1c1c1c', paddingBottom: 10}}>
        Masukkan Todolist
      </Text>
      <View style={styles.textInputContainer}>
        <TextInput
          style={styles.textInput}
          multiline={true}
          //   onChangeText={(value) => state.handleChangeInput(value)}
          // sepertinya kalau dalam arrow function kayak gini gak apa2 langsung setValue
          // tapi kalau d fungsi handleDelete, saya pindahkan ke sini , error
          onChangeText={(value) => state.setValue(value)}
          placeholder={'input here'}
          placeholderTextColor="#a9a9a9"
          color="#1c1c1c"
          value={state.value}
        />
        <TouchableOpacity onPress={() => state.handleAddTodo()}>
          <View style={styles.plusIcon}>
            <Icon name="plus" size={30} color="#1c1c1c" />
          </View>
        </TouchableOpacity>
      </View>

      <FlatList
        data={state.todos}
        renderItem={renderItem}
        keyExtractor={state.todos.key}
      />
    </View>
  );
};

export default TodoList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 15,
  },
  plusIcon: {
    backgroundColor: '#3dc6ff',
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textInput: {
    flex: 1,
    minHeight: '7%',
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: '#1c1c1c',
    marginRight: 10,
  },
  textInputContainer: {
    flexDirection: 'row',
  },
  taskWrapper: {
    marginTop: '5%',
    flexDirection: 'row',
    borderColor: '#c9c9c9',
    borderWidth: 4,
    width: '100%',
    alignItems: 'stretch',
    minHeight: 40,
    borderRadius: 6,
  },
  lisTask: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '90%',
  },
  task: {
    padding: 10,
    borderColor: '#1c1c1c',
    color: '#1c1c1c',
  },
  iconTrash: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
