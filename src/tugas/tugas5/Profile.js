import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Axios from 'axios';
import Asyncstorage from '@react-native-community/async-storage';
import api from '../../api';
import {GoogleSignin} from '@react-native-community/google-signin';
import {CommonActions} from '@react-navigation/native';

const {height, width} = Dimensions.get('window');

const Profile = ({navigation}) => {
  const [userInfo, setUserInfo] = useState(null);

  useEffect(() => {
    async function getToken() {
      try {
        const token = await Asyncstorage.getItem('token');
        // return getAvenue(token);
        // console.log('GetToken -> token', token);
      } catch (err) {
        console.log(err);
      }
    }
    getToken();
    getCurrentUser();
  }, [userInfo]); // ? saya tambah krn liat d video sign in google

  const getCurrentUser = async () => {
    const userInfo = await GoogleSignin.signInSilently();
    // console.log('getCurrentUser -> userInfo', userInfo);
    setUserInfo(userInfo);
  };

  // const getAvenue = (token) => {
  //   Axios.get(`${api}/venues`, {
  //     timeout: 20000,
  //     //web yang dituju butuh Authorization header
  //     headers: {
  //       Authorization: 'Bearer' + token,
  //     },
  //   })
  //     .then((res) => {
  //       console.log('Profile -> res', res);
  //     })
  //     .catch((err) => {
  //       console.log('Profile -> err', err);
  //     });
  // };

  const onLogoutPress = async () => {
    try {
      //revoke google signin
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      await Asyncstorage.removeItem('token');
      navigation.navigate('Login');
      navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{name: 'Login'}],
        }),
      );
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.top}>
        <ProfilPict userInfo={userInfo} />
      </View>

      <View style={styles.descWrapper}>
        <ProfilData title="Tanggal Lahir" desc="08 Maret 1987" />
        <ProfilData title="Jenis Kelamin" desc="Laki - laki" />
        <ProfilData title="Hobi" desc="Ngoding" />
        <ProfilData title="No. Telp" desc="085255180807" />
        <ProfilData
          title="Email"
          desc={userInfo && userInfo.user && userInfo.user.email}
        />
        <View style={styles.btnContainer}>
          <TouchableOpacity
            style={styles.logout}
            onPress={() => onLogoutPress()}>
            <Text style={{color: '#FFFFFF'}}>LOGOUT</Text>
          </TouchableOpacity>
        </View>
      </View>

      <View style={styles.bottom}></View>
    </View>
  );
};

const ProfilPict = ({userInfo}) => {
  return (
    <View style={{alignItems: 'center'}}>
      <Image
        // source={{uri: 'http://placeimg.com/100/100/people'}}
        source={{uri: userInfo && userInfo.user && userInfo.user.photo}}
        style={styles.img}
      />
      <Text style={styles.name}>
        {userInfo && userInfo.user && userInfo.user.name}
      </Text>
    </View>
  );
};

const ProfilData = ({title, desc}) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        // backgroundColor: 'red',
      }}>
      <Text style={styles.descText}>{title}</Text>
      <Text style={styles.descText}>{desc}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  top: {
    height: height * 0.4,
    backgroundColor: '#3EC6FF',
    justifyContent: 'center',
    // position: 'relative',
  },
  img: {
    width: height * 0.15,
    height: height * 0.15,
    borderRadius: (height * 0.15) / 2,
  },
  name: {
    padding: width / 30,
    fontWeight: 'bold',
    fontSize: width / 17,
    color: '#EFEFEF',
  },
  descWrapper: {
    zIndex: 1,
    height: height * 0.35,
    width: width * 0.9,
    position: 'absolute',
    alignSelf: 'center',
    top: width / 2,
    borderRadius: width / 68,
    backgroundColor: '#EFEFEF',
    // backgroundColor: 'blue',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,

    elevation: 7,
  },
  descText: {
    fontSize: width / 28,
    paddingHorizontal: width / 20,
    paddingVertical: width / 50,
  },
  bottom: {
    flex: 1,
    backgroundColor: '#EFEFEF',
  },
  btnContainer: {
    zIndex: 2,
    backgroundColor: '#3EC6FF',
    height: width / 10,
    width: width * 0.8,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 6,
    alignSelf: 'center',
  },
  logout: {
    zIndex: 3,
  },
});

export default Profile;
