import React from 'react';
import {View, Text, Dimensions, StyleSheet} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import MenuItem from '../../components/MenuItem';
import SummaryItem from '../../components/SummaryItem';

const {height, width} = Dimensions.get('window');

const Home = () => {
  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.cardContainer}>
          <View style={styles.titleContainer}>
            <Text style={styles.menuTitle}>Kelas</Text>
          </View>
          <View style={styles.menuContainer}>
            <View style={styles.menuItem}>
              <MenuItem title="React Native" icon="react" type="mcicon" />
              <MenuItem title="Data Science" icon="logo-python" type="icon" />
              <MenuItem title="React JS" icon="logo-react" type="icon" />
              <MenuItem title="Laravel" icon="logo-laravel" type="icon" />
            </View>
          </View>
        </View>

        <View style={styles.cardContainer}>
          <View style={styles.titleContainer}>
            <Text style={styles.menuTitle}>Kelas</Text>
          </View>
          <View style={styles.menuContainer}>
            <View style={styles.menuItem}>
              <MenuItem title="Worpress" icon="wordpress" type="mcicon" />
              <MenuItem title="Design Grafis" icon="" type="dgicon" />
              <MenuItem title="Web Server" icon="server" type="mcicon" />
              <MenuItem title="UI/UX Design" icon="" type="uxicon" />
            </View>
          </View>
        </View>

        <View style={styles.summaryContainer}>
          <View style={styles.titleContainer}>
            <Text style={styles.menuTitle}>Summary</Text>
          </View>
          <SummaryItem title="React Native" today="20" total="100" />
          <SummaryItem title="Data Science" today="30" total="100" />
          <SummaryItem title="React JS" today="66" total="100" />
          <SummaryItem title="Laravel" today="60" total="100" />
          <SummaryItem title="Wordpress" today="20" total="100" />
          <SummaryItem title="Design Grafis" today="30" total="100" />
          <SummaryItem title="Web Server" today="66" total="100" />
          <SummaryItem title="UI/UX Design" today="60" total="100" />
          <View style={styles.footerSummary} />
        </View>
      </ScrollView>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: '1%',
  },
  cardContainer: {
    marginBottom: '2%',
  },
  titleContainer: {
    backgroundColor: '#088dc4',
    borderTopRightRadius: 6,
    borderTopLeftRadius: 6,
  },
  menuTitle: {
    padding: '1%',
    color: 'white',
    marginLeft: '1%',
  },
  menuContainer: {
    backgroundColor: '#3EC6FF',
    borderBottomLeftRadius: 6,
    borderBottomRightRadius: 6,
  },
  menuItem: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  summaryContainer: {
    borderRadius: 6,
    marginBottom: '1.5%',
    flex: 1,
  },
  footerSummary: {
    height: 20,
    width: '100%',
    backgroundColor: '#3EC6FF',
    borderBottomLeftRadius: 6,
    borderBottomRightRadius: 6,
  },
});
