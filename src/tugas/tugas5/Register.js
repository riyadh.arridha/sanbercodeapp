import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Modal,
  StatusBar,
} from 'react-native';

import Icon from 'react-native-vector-icons/Feather';
import storage from '@react-native-firebase/storage';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import {RNCamera} from 'react-native-camera';

const {height, width} = Dimensions.get('window');

const Register = ({navigation}) => {
  const [nama, setNama] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [isVisible, setIsVisible] = useState(false);
  const [type, setType] = useState('back');
  const [photo, setPhoto] = useState(null);

  const toggleCamera = () => {
    setType(type === 'back' ? 'front' : 'back');
  };

  const takePicture = async () => {
    const options = {quality: 0.5, base64: true};
    if (camera) {
      const data = await camera.takePictureAsync(options);
      console.log('takePicture -> data', data);
      setPhoto(data);
      setIsVisible(false);
    }
  };

  const uploadImage = (uri) => {
    const sessionId = new Date().getTime();
    return storage()
      .ref(`image/${sessionId}`)
      .putFile(uri)
      .then((response) => {
        alert('Upload Success');
      })
      .catch((error) => {
        alert(error);
      });
  };

  const renderCamera = () => {
    // console.log('isVisible ->', isVisible);
    return (
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <View style={{flex: 1}}>
          <RNCamera
            style={{flex: 1}}
            ref={(ref) => {
              camera = ref;
            }}
            type={type}>
            <View style={styles.btnFlipContainer}>
              <TouchableOpacity
                style={styles.btnFlip}
                onPress={() => toggleCamera()}>
                <MaterialCommunity name="rotate-3d-variant" size={15} />
              </TouchableOpacity>
            </View>
            <View style={styles.round} />
            <View style={styles.rectangle} />
            <View style={styles.btnTakeContainer}>
              <TouchableOpacity
                style={styles.btnTake}
                onPress={() => {
                  takePicture();
                }}>
                <Icon name="camera" size={30} />
              </TouchableOpacity>
            </View>
          </RNCamera>
        </View>
      </Modal>
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.top}>
        <View style={{alignItems: 'center'}}>
          <Image
            source={
              photo === null
                ? {uri: 'http://placeimg.com/100/100/people'}
                : {uri: photo.uri}
            }
            // source={{uri: userInfo && userInfo.user && userInfo.user.photo}}
            style={styles.img}
          />
          <TouchableOpacity onPress={() => setIsVisible(true)}>
            <Text style={styles.name}>Change Picture</Text>
          </TouchableOpacity>
        </View>
      </View>

      <View style={styles.descWrapper}>
        <View style={styles.formatContainer}>
          <Text>Nama</Text>
          <TextInput
            value={nama}
            underlineColorAndroid="#c6c6c6"
            placeholder="Name"
            onChangeText={(nama) => setNama(nama)}
          />
          <Text>Email</Text>
          <TextInput
            value={email}
            underlineColorAndroid="#c6c6c6"
            placeholder="Email"
            onChangeText={(email) => setEmail(email)}
          />
          <Text>Password</Text>
          <TextInput
            value={password}
            secureTextEntry
            placeholder="Password"
            underlineColorAndroid="#c6c6c6"
            onChangeText={(password) => setPassword(password)}
          />
        </View>
        <View style={styles.btnContainer}>
          <TouchableOpacity
            style={styles.register}
            onPress={() => uploadImage(photo.uri)}>
            <Text style={{color: '#FFFFFF'}}>REGISTER</Text>
          </TouchableOpacity>
        </View>
      </View>

      <View style={styles.bottom}></View>
      {renderCamera()}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  top: {
    height: height * 0.4,
    backgroundColor: '#3EC6FF',
    justifyContent: 'center',
    // position: 'relative',
  },
  img: {
    width: height * 0.15,
    height: height * 0.15,
    borderRadius: (height * 0.15) / 2,
  },
  name: {
    padding: width / 30,
    fontWeight: 'bold',
    fontSize: width / 27,
    color: '#EFEFEF',
  },
  descWrapper: {
    zIndex: 1,
    height: height * 0.42,
    width: width * 0.9,
    position: 'absolute',
    alignSelf: 'center',
    top: 230,
    borderRadius: width / 68,
    backgroundColor: '#EFEFEF',
    // backgroundColor: 'blue',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,

    elevation: 7,
  },
  descText: {
    fontSize: width / 28,
    paddingHorizontal: width / 20,
    paddingVertical: width / 50,
  },
  bottom: {
    flex: 1,
    backgroundColor: '#EFEFEF',
  },
  btnContainer: {
    zIndex: 2,
    backgroundColor: '#3EC6FF',
    height: width / 10,
    width: width * 0.8,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 6,
    alignSelf: 'center',
  },
  register: {
    zIndex: 3,
  },
  formatContainer: {
    padding: 10,
  },
  btnFlipContainer: {
    height: 50,
    width: 50,
    borderRadius: 25,
    backgroundColor: 'white',
    marginTop: 20,
    marginLeft: 20,
    borderWidth: 1,
    borderColor: 'white',
    position: 'relative',
    justifyContent: 'center',
  },
  btnFlip: {
    // marginTop: 15,
    // marginLeft: 15,
    position: 'absolute',
    alignSelf: 'center',
  },
  round: {
    height: 280,
    width: 200,
    borderRadius: 100,
    borderWidth: 1,
    borderColor: 'white',
    alignSelf: 'center',
  },
  rectangle: {
    height: 120,
    width: 200,
    borderColor: 'white',
    borderWidth: 1,
    alignSelf: 'center',
    marginTop: 50,
  },
  btnTakeContainer: {
    height: 80,
    width: 80,
    borderRadius: 40,
    borderColor: 'white',
    borderWidth: 1,
    backgroundColor: 'white',
    position: 'relative',
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 30,
  },
  btnTake: {
    // justifyContent: 'center',
    position: 'absolute',
    alignSelf: 'center',
  },
});

export default Register;
