import {StyleSheet, Dimensions} from 'react-native';

const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  slide: {
    justifyContent: 'center',
    alignItems: 'center',
    height: height * 0.8,
  },
  title: {
    color: '#003366',
    fontSize: width / 25,
    fontWeight: 'bold',
    padding: width / 20,
  },
  text: {
    color: '#969898',
    fontWeight: 'bold',
    padding: width / 20,
    textAlign: 'center',
  },
  buttonCircle: {
    backgroundColor: '#003366',
    height: width / 10,
    width: width / 10,
    borderRadius: width / 10 / 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageContainer: {
    alignSelf: 'center',
  },
  content: {
    marginHorizontal: 20,
  },
  formatContainer: {
    marginVertical: 15,
  },
  btnContainer: {
    borderRadius: 6,
    marginTop: 10,
  },
  lineContainer: {
    flexDirection: 'row',
    // backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 15,
  },
  line: {
    height: 1,
    color: '#1c1c1c',
    borderBottomWidth: 1,
    width: width * 0.42,
    justifyContent: 'center',
  },
  footer: {
    alignItems: 'center',
    height: width / 4,
    width: width * 0.9,
    justifyContent: 'flex-end',
  },
  loading: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;
