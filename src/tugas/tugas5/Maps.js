import React, {useEffect, useState} from 'react';
import {View, Text} from 'react-native';
import MapboxGL from '@react-native-mapbox-gl/maps';

//set access token Mapbox
MapboxGL.setAccessToken(
  'pk.eyJ1Ijoicml5YWRoYXJyaWRoYSIsImEiOiJja2RmZTFwZTYwZnNsMnJ0MXNwdzUyejZlIn0.4JlHSGde45luCx1ck2x6qQ',
);
// const coordinate = [
//   [107.598827, -6.896191],
//   [107.596198, -6.899688],
//   [107.618767, -6.902226],
//   [107.621095, -6.89869],
//   [107.615698, -6.896741],
//   [107.613544, -6.897713],
//   [107.613697, -6.893795],
//   [107.610714, -6.891356],
//   [107.605468, -6.893124],
//   [107.60918, -6.898013],
// ];

const Location = () => {
  const [coordinates, setCoordinates] = useState([
    [107.598827, -6.896191],
    [107.596198, -6.899688],
    [107.618767, -6.902226],
    [107.621095, -6.89869],
    [107.615698, -6.896741],
    [107.613544, -6.897713],
    [107.613697, -6.893795],
    [107.610714, -6.891356],
    [107.605468, -6.893124],
    [107.60918, -6.898013],
  ]);

  const renderAnnotation = (counter) => {
    const id = `pointAnnotation${counter}`;
    const coordinate = coordinates[counter];
    const title = `Longitude: ${coordinates[counter][0]} Latitude: ${coordinates[counter][1]}`;
    return (
      <MapboxGL.PointAnnotation
        key={id}
        id={id}
        title={title}
        coordinate={coordinate}>
        <MapboxGL.Callout title={title} />
      </MapboxGL.PointAnnotation>
    );
  };

  const renderAnnotations = () => {
    const items = [];
    for (let i = 0; i < coordinates.length; i++) {
      items.push(renderAnnotation(i));
    }
    return items;
  };

  return (
    <View style={{flex: 1}}>
      <MapboxGL.MapView style={{flex: 1}}>
        <MapboxGL.UserLocation visible={true} />
        <MapboxGL.Camera followUserLocation={true} />
        {renderAnnotations()}
      </MapboxGL.MapView>
    </View>

    // <View style={{flex: 1}}>
    //   <MapboxGL.MapView
    //   ref={(c) => this._map = c}
    //   style={{flex: 1}}
    //   zoomLevel={11}
    //   showUserLocation={true}
    //   userTrackingMode={1}
    //   centerCoordinate={this.state.coordinates[0]}>
    //     {this.renderAnnotations()}
    //   </MapboxGL.MapView>
    // </View>

    // <View style={{flex: 1}}>
    //   <MapboxGL.MapView style={{flex: 1}}>
    //     <MapboxGL.UserLocation visible={true} />
    //     <MapboxGL.Camera followUserLocation={true} />
    //     <MapboxGL.PointAnnotation
    //       id="pointAnnotation"
    //       coordinate={[108.4, -7.3]}>
    //       {/* //   coordinate={coordinates[0]}> */}
    //       <MapboxGL.Callout title="Ini adalah callout" />
    //     </MapboxGL.PointAnnotation>
    //   </MapboxGL.MapView>
    // </View>
  );
};

export default Location;
