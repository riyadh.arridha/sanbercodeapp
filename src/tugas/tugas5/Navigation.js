import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SplashScreen from './Splashscreen';
import Intro from './Intro';
import Login from './Login';
import Profile from './Profile';
import Register from './Register';
import Home from './Home';
import Location from './Maps';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const MainNavigation = () => (
  <Stack.Navigator initialRouteName="Intro">
    <Stack.Screen
      name="Intro"
      component={Intro}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Login"
      component={Login}
      options={{headerShown: false}}
    />
    {/* <Stack.Screen
      name="Profile"
      component={Profile}
      options={{headerShown: false}}
    /> */}
    <Stack.Screen
      name="Register"
      component={Register}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Home"
      component={TabNavigation}
      options={{headerShown: false}}
    />
  </Stack.Navigator>
);

const TabNavigation = () => (
  <Tab.Navigator
    screenOptions={({route}) => ({
      tabBarIcon: ({focused, color, size}) => {
        let iconName;

        if (route.name === 'Home') {
          iconName = focused ? 'home' : 'home-outline';
        } else if (route.name === 'Profile') {
          iconName = focused
            ? 'information-circle'
            : 'information-circle-outline';
        } else if (route.name === 'Location') {
          iconName = focused ? 'map' : 'map-outline';
        }
        // You can return any component that you like here!
        return <Ionicons name={iconName} size={size} color={color} />;
      },
    })}
    tabBarOptions={{
      activeTintColor: '#088dc4',
      inactiveTintColor: 'gray',
    }}>
    <Tab.Screen name="Home" component={Home} options={{headerShown: false}} />
    <Tab.Screen
      name="Location"
      component={Location}
      options={{headerShown: false}}
    />
    <Tab.Screen
      name="Profile"
      component={Profile}
      options={{headerShown: false}}
    />
  </Tab.Navigator>
);

function AppNavigation() {
  const [isLoading, setIsLoading] = React.useState(true);

  //mengatur durasi splashscreen saat aplikasi pertama kali dibuka
  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(!isLoading);
    }, 2000);
  }, []);

  if (isLoading) {
    return <SplashScreen />;
  }

  return (
    <NavigationContainer>
      <MainNavigation />
    </NavigationContainer>
  );
}

export default AppNavigation;
