import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  Button,
  TextInput,
  StatusBar,
  TouchableOpacity,
  Alert,
  StyleSheet,
  Dimensions,
  ToastAndroid,
  ActivityIndicator,
  ScrollView,
} from 'react-native';
import styles from './style';
import Axios from 'axios';
import api from '../../api';
import auth from '@react-native-firebase/auth';
import {CommonActions} from '@react-navigation/native';
import Asyncstorage from '@react-native-community/async-storage';
import {
  GoogleSignin,
  statusCodes,
  GoogleSigninButton,
} from '@react-native-community/google-signin';
import TouchID from 'react-native-touch-id';

const {width, height} = Dimensions.get('window');

const config = {
  title: 'Authentication Required',
  imageColor: '#191970',
  imageErrorColor: 'red',
  sensorDescription: 'Touch Sensor',
  sensorErrorDescription: 'Failed',
  cancelText: 'Cancel',
};

function Login({navigation}) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const saveToken = async (token) => {
    try {
      await Asyncstorage.setItem('token', token);
    } catch (err) {
      console.log(err);
      ToastAndroid.show('Gagal menyimpan token', ToastAndroid.SHORT);
    }
  };

  useEffect(() => {
    configureGoogleSignIn();
  }, []);

  //konfigurasi google
  const configureGoogleSignIn = () => {
    GoogleSignin.configure({
      webClientId:
        '331199580211-ai6vfd5gms7m24gbtp7shg8r5ijkh1oc.apps.googleusercontent.com',
      offlineAccess: false, //gak harus
    });
  };

  const signInWithGoogle = async () => {
    try {
      const {idToken} = await GoogleSignin.signIn();
      console.log('signInWithGoogle -> idToken', idToken);
      //integrasi dg firebase
      const credential = auth.GoogleAuthProvider.credential(idToken);
      auth().signInWithCredential(credential);

      navigation.navigate('Home');
    } catch (error) {
      console.log('signInWithGoogle -> error', error);
    }
  };

  const onLoginPress = () => {
    let data = {
      email: email,
      password: password,
    };
    Axios.post(`${api}/login`, data, {
      timeout: 20000,
    })
      .then((res) => {
        // console.log('Login -> res', res);
        // saveToken(res.data.token);
        // navigation.navigate('Profile');
        setIsLoading(false);
        saveToken(res.data.token);
        navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [
              {
                name: 'Login',
              },
            ],
          }),
        );
      })
      .catch((err) => {
        // console.log('Login -> err', err);
        // ToastAndroid.show('Gagal melakukan autentikasi', ToastAndroid.SHORT);
        setIsLoading(false);
        setIsLoggedIn(false);
        Alert.alert('Error', err.message);
      });
  };

  const signInWithFingerPrint = () => {
    TouchID.authenticate('', config)
      .then((success) => {
        alert('Authentication Success');
        navigation.navigate('Home');
      })
      .catch((error) => {
        alert('Authentication Failed');
      });
  };

  if (isLoading) {
    return (
      <ActivityIndicator size="large" color="#3EC6FF" style={styles.loading} />
    );
  }

  return (
    <View style={styles.container}>
      <ScrollView>
        <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />
        <View style={styles.imageContainer}>
          <Image
            source={require('../../assets/images/logo.jpg')}
            style={styles.logo}
          />
        </View>
        <View style={styles.content}>
          <View style={styles.formatContainer}>
            <Text>Username</Text>
            <TextInput
              value={email}
              underlineColorAndroid="#c6c6c6"
              placeholder="Username or Email"
              onChangeText={(email) => setEmail(email)}
            />
            <Text>Password</Text>
            <TextInput
              value={password}
              secureTextEntry
              placeholder="Password"
              underlineColorAndroid="#c6c6c6"
              onChangeText={(password) => setPassword(password)}
            />
          </View>
          <View style={styles.btnContainer}>
            <Button
              color="#3EC6FF"
              title="LOGIN"
              onPress={() => onLoginPress()}
            />
          </View>
          <View style={styles.lineContainer}>
            <View style={styles.line}></View>
            <Text style={{alignItems: 'center'}}> OR </Text>
            <View style={styles.line}></View>
          </View>
          <View style={styles.btnContainer}>
            <GoogleSigninButton
              onPress={() => signInWithGoogle()}
              style={{width: '100%', height: 40}}
              size={GoogleSigninButton.Size.Wide}
              color={GoogleSigninButton.Color.Dark}
            />
          </View>
          <View style={styles.btnContainer}>
            <Button
              color="#191970"
              title="SIGN IN WITH FINGERPRINT"
              onPress={() => signInWithFingerPrint()}
            />
          </View>

          <View style={styles.footer}>
            <View style={{flexDirection: 'row'}}>
              <Text>Belum mempunyai akun? </Text>
              <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                <Text style={{color: '#003366'}}>Buat Akun</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

export default Login;
