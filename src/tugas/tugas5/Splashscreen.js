import React from 'react';
import {View, Image, StyleSheet, Dimensions} from 'react-native';

const {height, width} = Dimensions.get('window');

function SplashScreen() {
  return (
    <View style={styles.container}>
      <View style={styles.logoContainer}>
        <Image source={require('../../assets/images/logo.jpg')} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ffffff',
  },
  logoContainer: {
    padding: width / 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default SplashScreen;
