import Intro from './tugas1/intro';
import Biodata from './tugas2/biodata';
import TodoList from './tugas3/App';
import TodoContext from './tugas4';
import Splash from './tugas5/Navigation';
import Profile from './tugas5/Profile';

export {Intro, Biodata, TodoList, TodoContext, Splash, Profile};
