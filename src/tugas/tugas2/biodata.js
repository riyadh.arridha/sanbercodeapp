import React from 'react';
import {View, Text, Image, Dimensions, StyleSheet} from 'react-native';

const {height, width} = Dimensions.get('window');

const Biodata = () => {
  //   console.log('Width', width);
  //   console.log('Height', height);
  return (
    <View style={styles.container}>
      <View style={styles.top}>
        <ProfilPict />
      </View>

      <View style={styles.descWrapper}>
        <ProfilData title="Tanggal Lahir" desc="08 Maret 1987" />
        <ProfilData title="Jenis Kelamin" desc="Laki - laki" />
        <ProfilData title="Hobi" desc="Ngoding" />
        <ProfilData title="No. Telp" desc="085255180807" />
        <ProfilData title="Email" desc="riyadh.arridha@gmail.com" />
      </View>

      <View style={styles.bottom}></View>
    </View>
  );
};

const ProfilPict = () => {
  return (
    <View style={{alignItems: 'center'}}>
      <Image
        source={{uri: 'http://placeimg.com/100/100/people'}}
        style={styles.img}
      />
      <Text style={styles.name}>Riyadh Arridha</Text>
    </View>
  );
};

const ProfilData = ({title, desc}) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
      }}>
      <Text style={styles.descText}>{title}</Text>
      <Text style={styles.descText}>{desc}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  top: {
    height: height * 0.4,
    backgroundColor: '#3EC6FF',
    justifyContent: 'center',
  },
  img: {
    width: height * 0.15,
    height: height * 0.15,
    borderRadius: (height * 0.15) / 2,
  },
  name: {
    padding: width / 30,
    fontWeight: 'bold',
    fontSize: width / 17,
    color: '#EFEFEF',
  },
  descWrapper: {
    height: height * 0.3,
    width: width * 0.9,
    position: 'absolute',
    alignSelf: 'center',
    borderRadius: width / 68,
    backgroundColor: '#EFEFEF',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,

    elevation: 7,
  },
  descText: {
    fontSize: width / 28,
    paddingHorizontal: width / 20,
    paddingVertical: width / 50,
  },
  bottom: {
    flex: 1,
    backgroundColor: '#EFEFEF',
  },
});

export default Biodata;
