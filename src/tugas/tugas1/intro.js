import React from 'react';
import {View, Text} from 'react-native';

const Intro = () => {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#e0e0e0',
      }}>
      <Text>Hallo Kelas React Native Lanjutan Sanbercode!</Text>
    </View>
  );
};

export default Intro;
