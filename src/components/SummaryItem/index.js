import React from 'react';
import {View, Text, Dimensions, StyleSheet} from 'react-native';

const {height, width} = Dimensions.get('window');

const SummaryItem = ({title, today, total}) => {
  return (
    <View>
      <View style={styles.subTitleContainer}>
        <Text style={styles.subMenuTitle}>{title}</Text>
      </View>
      <View style={styles.listSummaryContainer}>
        <View style={styles.listSummaryItem}>
          <Text style={styles.textSummary}>Today</Text>
          <Text style={styles.textSummary}>{today} orang</Text>
        </View>
        <View style={styles.listSummaryItem}>
          <Text style={styles.textSummary}>Total</Text>
          <Text style={styles.textSummary}>{total} orang</Text>
        </View>
      </View>
    </View>
  );
};

export default SummaryItem;

const styles = StyleSheet.create({
  subTitleContainer: {
    backgroundColor: '#3EC6FF',
  },
  subMenuTitle: {
    padding: '1%',
    color: 'white',
    marginLeft: '1%',
    fontWeight: 'bold',
  },
  listSummaryContainer: {
    backgroundColor: '#088dc4',
  },
  listSummaryItem: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  textSummary: {
    color: 'white',
  },
});
