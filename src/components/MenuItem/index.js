import React from 'react';
import {View, Text, Dimensions, StyleSheet, Image} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import MCIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const {height, width} = Dimensions.get('window');

const MenuItem = ({title, icon, type}) => {
  function checkType(type) {
    if (type === 'mcicon') {
      return <MCIcons name={icon} size={width / 10} color="white" />;
    } else if (type === 'icon') {
      return <Icon name={icon} size={width / 10} color="white" />;
    } else if (type === 'uxicon') {
      return (
        <Image
          source={require('./../../assets/images/ux.png')}
          style={{height: width / 10, width: width / 10}}
        />
      );
    } else if (type === 'dgicon') {
      return (
        <Image
          source={require('./../../assets/images/website-design.png')}
          style={{height: width / 10, width: width / 10}}
        />
      );
    }
  }

  return (
    <View style={styles.menu}>
      {checkType(type)}
      <Text style={styles.menuText}>{title}</Text>
    </View>
  );
};

export default MenuItem;
const styles = StyleSheet.create({
  menu: {
    height: width / 5,
    width: width / 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  menuText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 12,
  },
});
