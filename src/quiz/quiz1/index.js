import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet} from 'react-native';

const Quiz1 = () => {
  const [name, setName] = useState('Jhon Doe');
  useEffect(() => {
    setTimeout(() => {
      setName('Zakky');
    }, 2000);
    return () => {
      setName('Mukhlis');
    };
  }, [name]);
  return (
    <View style={styles.container}>
      <Text>{name}</Text>
    </View>
  );
};

export default Quiz1;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
